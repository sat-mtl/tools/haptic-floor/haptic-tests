# haptic-tests

This repo is a collection of scripts, patches and ideas for testing and playing with the haptic floor.

Developed by the Society for Arts and Technology [SAT] in Montreal, this haptic floor device consists of a mesh of triangular tiles whose 
vertices are mobile. The tiles can vibrate, shake, move and react to sounds. The haptic floor makes it possible to create immersive and 
interactive experiences in places of varying dimensions, opening up new perspectives in the field of entertainment (video games, amusement 
parks), culture (art installations, museums, sciences…) and industry (advanced simulations, architecture).
