from pythonosc import udp_client
import json
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("ascii_art",
    help="path to the json ascii_art file")
parser.add_argument("--gain", type=float, default=0.5,
    help="The ip of the OSC server")
parser.add_argument("--control-rate", type=int, default=10,
    help="The ip of the OSC server")
parser.add_argument("--ip", default="localhost",
    help="The ip of the OSC server")
parser.add_argument("--port", type=int, default=9000,
    help="The port the OSC server is listening on")
parser.add_argument("--channels", type=int, default=4,
    help="number of channels the floor we send to is currently using")
args = parser.parse_args()

client = udp_client.SimpleUDPClient(args.ip, args.port)
lines = []

with open(args.ascii_art) as signal_file:
    lines = signal_file.read().split("\n")

data_lines = []
# max numeric amount found in the text file
max_ord = 0
# minimum numeric amount found in the text file
min_ord = float("inf")

# Convert every character in a number
for line in filter(None, lines):
    data_line = []
    for character in line:
        numeric = ord(character)
        max_ord = max(numeric, max_ord)
        min_ord = min(numeric, min_ord)
        data_line.append(numeric)
    data_lines.append(data_line)

def rescale(number):
    """takes a number betwee min_ord and max_ord and rescale it
    to go from -1 to 1"""
    # range of possible values with offset removed
    number_range = max_ord - min_ord
    # magnitude of the current values with offset removed
    magnitude = number - min_ord
    # magnitude normalized between 0 and 1
    normalized = magnitude/number_range
    # scaled to -1 to 1
    return (normalized * 2) - 1

# converts integers from min_ord to max_ord to floats from -1 to 1
transformed_data_lines = [[rescale(data) for data in data_line] for data_line in data_lines]

# interval that should be waited before sending each osc packet.
dt = 1/args.control_rate

# index of the currently read line
line_idx = 0
# index of the currently read character
column_idx = 0

# Note : this will not work on text with variable line width
# length of a line
line_len = len(transformed_data_lines)
# length of a column
col_len = len(transformed_data_lines[0])

def print_char(x, y, charac):
    """
    prints the specified character at the specified x and y coordinate on the terminal screen
    thanks stackoverflow : https://stackoverflow.com/a/50754123
    """
    print("\033["+str(y)+";"+str(x)+"H"+charac)

while start := time.time():
## sends the next sample to all channels
    positions = []
    # for every channel, read a number on the line corresponding to line_idx + channel
    for i in range(args.channels):
        line_index = (line_idx+i)%line_len
        positions.append(transformed_data_lines[line_index][column_idx])
        # also display the character in the terminal. Line number for the terminal function starts at 1 and not 0.
        print_char(column_idx, line_index+1, lines[line_index][column_idx])
        # ic(column_idx)
        # ic(line_index+1)
    # increment the column index
    column_idx = (column_idx + 1) % col_len
    # if our column index looped back to 0, increment the line index too.
    if column_idx == 0:
        new_line_idx = (line_idx + args.channels)%line_len
        if new_line_idx < line_idx:
            #clears screen
            print(chr(27) + "[2J")
        line_idx = new_line_idx

    # scale the position by the gain argument. 1 often is too intense.
    scaled_positions = [position * args.gain for position in positions]
    client.send_message("/signal", scaled_positions)
    # wait until its time for another frame
    while True:
        if(time.time() - start) > dt:
            break
        continue
