# Python examples

This folder contains more "artsy" examples of using python to communicate with the haptic floor.

## ascii-art-to-haptics.py

This script reads a text file containing ascii art and will convert the ascii values to numbers from -1 to 1 and send them as osc values that the haptic floor can understand.

From our experimentation, the results are better when sending at a low control rate (+- 10hz) with a gain of about 0.5. If sending at less than the native sampling rate of the floor, you will need to use the resampler option of mmesh.
