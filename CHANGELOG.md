Release Notes
===================

haptic-tests 1.0.0 (2023-07-21)
---------------------------------

Release of all latests tests with the haptic floor!

* ✨ Add pipe to OSC tool
* ✨ Adds ascii art example
* ✨ Add new TD examples
* ✨ Add new Touchdesigner examples
* ✨ Makes the visualisation script graph the jerk value for a signal
* 🩹 Change TouchDesigner haptic-fun patch to OSC float message
* 🐛 Puts max travel as the max X
* ✨ Add Chataigne OSC tests
* ✨ Adds a script to make the floor move using a computer keyboard.
* 🔢 Adapts visualization script with the right constants
* ✨ Add amplitude control to test patches
* ♻️ refactor test patches to use sub patches and delete unnecessary files
* ✨Adds test signals and python scripts to manipulate them
* 🎨 Remove bridge dependence and refactor osc visualizer
* 🎨 Make visualizer graph all signals to same graph and remove dependency on the...
* 🔨 Update Touchdesigner patches
* 🎨 Add pure data puredata osc data visualizer
* ✨ Add a simple sine wave generator with a noise generator with Pure Data
* ✨ Adds two possible ways of replicating the numpy scripts birds and waves
* ✨ Add a patch that converts 4 sine wave oscillators to 4 floats
* ✨ Add a minimum working puredata patch for example purposes
* ✨ Revise array_osc_send to work with numpy bridge and add experiments
* ✨ Add TouchDesigner patches
* ✨ Add a puredata patch that allows you to draw positionals directly by hand
* ✨ Add a simple puredata patch to test the haptic floor that does not work
