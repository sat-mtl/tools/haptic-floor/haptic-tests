import argparse
import numpy
import json
import scipy.io.wavfile as wf

parser = argparse.ArgumentParser()
parser.add_argument("signal", help="Name of the signal json file.")
parser.add_argument("out", help="Name of the wav output file.")
parser.add_argument("--sampling-rate", type=float, nargs="?", default=400, const=400, help="sampling rate of the signal. Default 400Hz")

args = parser.parse_args()

samples = json.load(open(args.signal))
wf.write(args.out, 400, numpy.asarray(samples))
