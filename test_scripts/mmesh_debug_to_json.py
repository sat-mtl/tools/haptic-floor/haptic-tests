import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("signal_path", help="path of the file containing mmesh debug output")
parser.add_argument("out_path", help="path of the json signal file containing the first float channel of the debug input")

args = parser.parse_args()

mmesh_text = open(args.signal_path).read()
motion_json = []
for line in mmesh_text.split("\n"):
    # all of this is obviously super fragile and will break on mmesh debug format changes.
    if len(line.split(" ")) > 4 and line.split(" ")[4] == "motion:":
        # takes only the data from the first channel.
        motion_json.append(float(line.split("(")[1].split(",")[0]))

json.dump(motion_json, open(args.out_path, "w"))
