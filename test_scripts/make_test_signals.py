import argparse
import json
import math
from random import uniform

parser = argparse.ArgumentParser()
parser.add_argument("outfile", help="outfile name for the signal json file.")
parser.add_argument("--amp", type=float, nargs="?", default=1, help="factor applied to the signal. Default 1 (generates signal from -1 to 1)")
parser.add_argument("--shape",
                    default="square",
                    const="square",
                    nargs="?",
                    choices=["square", "triangle", "sine", "noise"],
                    help="Waveform of the signal to be generated. Default square")

parser.add_argument("--freq", type=float, nargs="?", default=0, const=1, help="initial frequency of the signal. defaults to 1Hz.")
parser.add_argument("--init-freq", type=float, nargs="?", default=1, const=1, help="initial frequency of the signal. defaults to 1Hz.")
parser.add_argument("--end-freq", type=float, nargs="?", default=1, const=1, help="end frequency of the signal, defaults to 1Hz.")
parser.add_argument("--length", type=float, nargs="?", default=3, const=3, help="Length of the signal in seconds. Default 3")
parser.add_argument("--sampling-rate", type=float, nargs="?", default=400, const=400, help="sampling rate of the signal. Default 400Hz")
parser.add_argument("--limit-amp", action='store_true',
                    help="""limits the amplitud of the waveform to make it so it doesn't surpass the limitation of the floor.
                    This option will only make a conforming signal with the sine shape and a fixed frequency.""")

args = parser.parse_args()

max_stroke = 0.038;
half_stroke = max_stroke / 2.0;
# Paramètres
g = 9.81 # m/s^2 (Gravité)
vMax = 100e-3 # m/s (Vitesse max D-Box)
max_acceleration_unitless = g / half_stroke
max_velocity_unitless = vMax / half_stroke;

def accel_to_amplitude(freq):
    """computes the amplitude of a sine wave at a given frequency so that the maximal acceleration conforms
    to our limis. This uses the harmonic motion formulas"""
    angular_velocity = 2*math.pi*freq
    amplitude = max_acceleration_unitless/(angular_velocity**2)
    return amplitude

def speed_to_amplitude(freq):
    """computes the amplitude of a sine wave at a given frequency so that the maximal velocity conforms
    to our limis. This uses the harmonic motion formulas"""
    angular_velocity = 2*math.pi*freq
    amplitude = max_velocity_unitless/angular_velocity
    return amplitude

def get_max_amplitude_for_current_freq(freq):
    """for a given frequency, returns the most restrictive limit between the
    acceleration and the speed limit."""
    return min(accel_to_amplitude(freq), speed_to_amplitude(freq))

def get_ramp(discrete_phase, sampling_rate):
    """convert timesteps in int to a float ramp from 0 to 1"""
    return (discrete_phase%sampling_rate)/sampling_rate

def get_square_sample(phase):
    """gets a square sample from a phase going from 0 to 1."""
    return -1 if phase < 0.5 else 1

def get_sine_sample(phase):
    """gets a sine sample from a phase going from 0 to 1"""
    # this could be simplified/optimized by making a wave that starts at -1 instead of 0 and then
    # dephase it internally but who cares.
    return math.sin(phase*2*math.pi)

def get_triangle_sample(phase):
    """gets a triangle sample from a phase going from 0 to 1"""
    # this could be simplified/optimized by making a wave that starts at -1 instead of 0 and then
    # dephase it internally but who cares.

    # for the first quarter of a phase, goes from 0 to 1 linearly
    if 0 <= phase <= 0.25:
        return phase*4
    # for the second and third quarter of a phase, goes from 1 to -1 linearly
    if 0.25 < phase <= 0.75:
        return 1 - ((phase - 0.25) * 4)
    # for the fourth quarter of a phase, goes from -1 to 0
    else:
        return ((phase - 0.75) * 4) - 1

def get_noise_sample(phase):
    return uniform(-1,1)

shape_funcs = {"square": get_square_sample, "triangle": get_triangle_sample, "sine": get_sine_sample, "noise": get_noise_sample}

def blend(x,y,blend):
    return (x * (1 - blend)) + (y * blend)

with open(args.outfile, "w") as outfile:
    signal = []
    signal_samples = round(args.length * args.sampling_rate)
    for i in range(signal_samples):
        # progression in a number from 0 to 1.
        progression = i/(signal_samples-1)
        # the actual frequency is a linear blend of the init_freq and the end_freq
        freq = 0
        if args.freq:
            freq = args.freq
        else:
            freq = blend(args.init_freq, args.end_freq, progression)
        phase = i*freq
        ramp = get_ramp(phase, args.sampling_rate)
        sample = shape_funcs[args.shape](ramp)
        if args.limit_amp:
            # if we are generated a limited sine, we want to limit the amplitude depending
            # on the frequency.
            sample *= (get_max_amplitude_for_current_freq(freq))
        else:
            sample *= args.amp
        signal.append(sample)
    json.dump(signal, outfile)
