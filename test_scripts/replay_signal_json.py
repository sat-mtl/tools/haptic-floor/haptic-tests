from pythonosc import udp_client
import json
import time
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("signal",
        help="path to the json signal file")
    parser.add_argument("--ip", default="localhost",
        help="The ip of the OSC server")
    parser.add_argument("--port", type=int, default=9000,
        help="The port the OSC server is listening on")
    parser.add_argument("--channels", type=int, default=4,
        help="number of channels the floor we send to is currently using")
    args = parser.parse_args()

    client = udp_client.SimpleUDPClient(args.ip, args.port)
    samples = []
    print(args.signal)
    with open(args.signal) as signal_file:
        samples = json.load(signal_file)
    frame = []
    dt = 1/400
    index = 0
    while start := time.time():
        # sends the next sample to all channels
        client.send_message("/signal", [samples[index]] * args.channels)
        # increment the index. This plays in a
        index = (index + 1) % len(samples)
        while True:
            if(time.time() - start) > dt:
                break
            continue
