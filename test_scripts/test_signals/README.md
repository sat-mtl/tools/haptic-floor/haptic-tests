# Test signals

The following test signals are currently in the project classified by the test cases they are used in


## [MMESH-8](https://app.qase.io/case/MMESH-8)
- `sine_2amp_1hz.json` : A 1hz sine that goes from -2 to 2

## [MMESH-9](https://app.qase.io/case/MMESH-9)
- `sine_sweep_0_200_hz_fullscale.json` : A sine wave with an amplitude of 1 that goes from 0 to 200hz in 10 second

## [MMESH-10](https://app.qase.io/case/MMESH-10)
- `white_noise_fullscale.json` : White noise that goes from -1 to 1

## [MMESH-11](https://app.qase.io/case/MMESH-11)
- `sine_3_hz_amp_limited.json` : A 3hz sine wave with its amplitude reduced so that it conforms with the haptic floor limits. Used
- `sine_3_hz_fullscale.json` : A 3Hz sine wave with an amplitude of 1.

## [MMESH-13](https://app.qase.io/case/MMESH-13)
- `sine_sweep_1_20_hz_fullscale.json` : A sine wave with an amplitude of 1 with a frequency that goes from 1 to 20hz.
- `sine_sweep_1_20_hz_amp_limited.json` :A sine wave with a frequency that goes from 1 to 20hz that has its amplitude reduced so it always conforms to the limits.
