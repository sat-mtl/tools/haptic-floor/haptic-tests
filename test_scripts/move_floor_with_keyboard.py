from pythonosc import udp_client
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--ip", default="localhost",
    help="The ip of the OSC server")
parser.add_argument("--port", type=int, default=9000,
    help="The port the OSC server is listening on")
parser.add_argument("--channels", type=int, default=4,
    help="number of channels the floor we send to is currently using")
args = parser.parse_args()

# instantiate osc client.
client = udp_client.SimpleUDPClient(args.ip, args.port)

def send(val):
    client.send_message("/signal", [val] * args.channels)


def _find_getch():
    """thanks stackoverflow : https://stackoverflow.com/a/21659588 """
    try:
        import termios
    except ImportError:
        # Non-POSIX. Return msvcrt's (Windows') getch.
        import msvcrt
        return msvcrt.getch

    # POSIX system. Create and return a getch that manipulates the tty.
    import sys, tty
    def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    return _getch

# getch is a function that will block until a key is pressed. It will return the char of the key.
getch = _find_getch()

val = 0
print("press a number key to send a float value from -1 to 1 on all channels")
print("press q to exit")
exited = False
while not exited:
    try:
        unparsed = getch()
        if(unparsed == "q"):
            print("exiting")
            exited = True
        # divides the number of the received key by 9, multiplies by 2 and add -1 in order to get a position
        # float between -1 and 1
        val = (float(unparsed)/9) * 2 - 1
        send(val)
    except:
        pass
