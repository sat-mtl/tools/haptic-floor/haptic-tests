#!/usr/bin/python3
from matplotlib import pyplot as plt
import matplotlib.lines as lines
import numpy as np
import os
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("signal_path", help="path of the json signal file")
parser.add_argument("--spectrum", action='store_true', help="if this argument is present, this will make a spectrum graph instead of the travel/speed/acceleration ones.")
parser.add_argument("--graph-limits", action='store_false', help="if this argument is present the limits will be graphed in red.")
parser.add_argument("--compare", help="path of a json signal file to compare to the first one")
parser.add_argument("--signal-scaling-factor", type=float, help="scaling factor of the signal. Default 0.019")
parser.add_argument("--signal-offset", type=float, help="offset added to the signal. Default 0.019")
parser.add_argument("--sampling-rate", type=float, help="sampling rate of the signal. Default 400hz")
parser.add_argument("--max-accel", type=float, help="max acceleration. Default 1g (9.807m/s^2)")
parser.add_argument("--max-speed", type=float, help="max speed. Default 0.1 (100mm/s)")
parser.add_argument("--max-travel", type=float, help="max travel. Default 0.0345/2")
parser.add_argument("--out", help="outfile name for the graphic")

args = parser.parse_args()


# max travel distance of a DBOX actuator is 0.0345m
max_stroke = 0.0345;
scaling_factor = max_stroke/2 if not args.signal_scaling_factor else args.signal_scaling_factor #meter per second squared
# this default offset will bring back the scaled values between 0 and 0.038
offset = max_stroke/2 if not args.signal_offset else args.signal_offset #meter per second squared
# default max travel is the maximal travel of a DBOX actuator
max_travel = max_stroke if not args.max_travel else args.max_travel #meter per second squared
# default max accel is 1g
max_acceleration = 9.807 if not args.max_accel else args.max_accel #meter per second squared
max_speed = 0.1 if not args.max_speed else args.max_speed #meter per second(m/s)
sample_rate = 400 if not args.sampling_rate else args.sampling_rate # Hz
sample_interval = 1/sample_rate


def parse_signal(signal_path, scale_and_offset=True):
    """return a tuple of (samples, sample_times)"""
    samples = []
    with open(signal_path) as signal_json:
        samples = np.asarray(list(map(lambda sample: float(sample), json.load(signal_json))))
    if scale_and_offset:
        samples *= scaling_factor
        samples += offset

    num_samples = len(samples)
    # gets the time associated to each sample for the x axis.
    sample_times = np.asarray([i*sample_interval for i in range(len(samples))])
    return (samples, sample_times)

def get_speed(x0, x1, t0, t1):
    """get speed for a discrete step of time. Thanks CTA for the function"""
    v = (x1-x0)/(t1-t0)
    return v

def get_accel(x0, x1, x2, t0, t1, t2):
    a = (x2 - 2*x1 + x0)/( (t2-t1)*(t1-t0) )
    return a

def get_jerk(a0, a1, t0, t1):
    """gets the jerk implied by a variation in acceleration."""
    j = (a1-a0)/(t1-t0)
    return j

def get_speed_accel_jerk(samples, sample_times):
    speed = np.zeros(len(samples))
    acceleration = np.zeros(len(samples))
    jerk = np.zeros(len(samples))

    for i in range(1,len(samples)):
        speed[i] = get_speed(samples[i-1], samples[i], sample_times[i-1], sample_times[i])

    for i in range(2, len(samples)):
        acceleration[i] = get_accel(samples[i-2], samples[i-1], samples[i], sample_times[i-2], sample_times[i-1], sample_times[i])

    for i in range(1, len(acceleration)):
        jerk[i] = get_jerk(acceleration[i-1], acceleration[i], sample_times[i-1], sample_times[i])

    return (speed, acceleration, jerk)

def make_basic_graphs(samples, sample_times, plot_max_travel=True, plot_max_speed=True, plot_max_accel=True, figure=None, axes=None):
    """Makes a basic graph with the signal, its speed and its acceleration"""
    if figure is None and axes is None:
        plt.style.use('seaborn-v0_8-whitegrid')
        figure, axes = plt.subplots(4, 1, figsize=(10,7.5), gridspec_kw={"hspace": .5})
    titles = ["position", "speed", "acceleration", "jerk"]
    xlabels = ["time (s)"] * 4
    ylabels = ["position (m)", "speed (m/s)", "acceleration (m/s²)", "jerk (m/s³)"]
    for i, axis in enumerate(axes):
        axis.set_title(titles[i])
        axis.set_xlabel(xlabels[i])
        axis.set_ylabel(ylabels[i])
        axis.grid()
    speed, acceleration, jerk = get_speed_accel_jerk(samples, sample_times)

    axes[0].plot(sample_times, samples, label="input")
    axes[1].plot(sample_times, speed, label="speed")
    axes[2].plot(sample_times, acceleration, label="acceleration")
    axes[3].plot(sample_times, jerk, label="jerk")


    if plot_max_travel:
        axes[0].plot(sample_times, [max_travel]*len(samples), color="red", label="max travel")
        axes[0].plot(sample_times, [0]*len(samples), color="red", label="max travel")


    if plot_max_speed:
        axes[1].plot(sample_times, [max_speed]*len(samples), color="red", label="max speed")
        axes[1].plot(sample_times, [-max_speed]*len(samples), color="red", label="min speed")

    if plot_max_accel:
        axes[2].plot(sample_times, [max_acceleration]*len(samples), color="red", label="max accel")
        axes[2].plot(sample_times, [-max_acceleration]*len(samples), color="red", label="min accel")
    for axis in axes:
        axis.legend(loc="upper right")
        axis.grid()
    return (figure, axes)

def make_spectrum_graphs(samples, figure=None, axes=None):
    """makes a graph of the spectrum of a signal. Magnitudes are unitless and are only proportional to signal strength."""
    if figure is None and axes is None:
        figure, axes = plt.subplots(1, 1, figsize=(10,7.5), gridspec_kw={"hspace": .5})
    axes.set_title("spectrum")
    axes.set_xlabel("frequency")
    axes.set_ylabel("magnitude")
    rfft_y = np.fft.rfft(samples)
    rfft_x = np.fft.rfftfreq(samples.size, sample_interval)
    # normalize with length of signal
    magnitude = np.abs(rfft_y)/len(samples)
    axes.plot(rfft_x, magnitude, label="input")
    return (figure, axes)

if not args.spectrum:
    samples, sample_times = parse_signal(args.signal_path)
    figure, axes = make_basic_graphs(samples, sample_times, args.graph_limits, args.graph_limits, args.graph_limits)
    if args.compare:
        comp_samples, comp_sample_times = parse_signal(args.compare)
        figure, axes = make_basic_graphs(comp_samples, comp_sample_times, False, False, False, figure, axes)
else:
    # don't scale and offset the signal if we only want the spectrum
    samples, sample_times = parse_signal(args.signal_path, False)
    figure, axes = make_spectrum_graphs(samples)
    if args.compare:
        comp_samples, comp_sample_times = parse_signal(args.compare)
        figure, axes = make_spectrum_graphs(comp_samples, figure, axes)


if args.out:
    plt.savefig(args.out)
plt.show()
